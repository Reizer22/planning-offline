//
//  CollectionVC.swift
//  Planning
//
//  Created by admin on 26.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CocoaLumberjack

protocol CollectionVCDelegate {
  func updateSelected(_ plan: Plan)
}

class CollectionVC: UIViewController {
  
  var zoom: ZoomSquare!
  
  @IBOutlet weak var zoomStep: UIStepper!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  @IBOutlet weak var editButton: UIBarButtonItem!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var optionView: UIView!
  @IBOutlet weak var preview: UIView!
  @IBOutlet weak var previewContentLabel: UILabel!
  @IBOutlet weak var previewDopContentLabel: UILabel!
  @IBOutlet weak var previewDopContentText: UITextView!
  @IBOutlet weak var previewContentText: UITextView!
  var delegate: CollectionVCDelegate!
  var createCell: CreateCollectionCell?
  var selectCell: SelectCollectionCell?
  var popoverView: UIView?
  var popoverPreView: UIView?
  var tapToCell:UITapGestureRecognizer!
  var headerDates = [Date]()
  var headerNames = [String]()
  var currentDateRow: Int?
  var scrollCollection: Bool = true
  var operationOnCell: String?
  
  var squares = [[Any]]()
  var selectedSquare: Square?
  var selectedIndexPath:IndexPath?
  var selectedPlan: Plan!
  
  //MARK: - Load
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    //Скролл до текущей даты, если есть
    if currentDateRow != nil && scrollCollection {
      self.collectionView.scrollToItem(at: IndexPath.init(row: currentDateRow!, section: 0), at: .centeredHorizontally, animated: true)
      scrollCollection = false
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //Удаление посторонних view на detail
    self.removeAnotherOpenViews()
    //Кнопка расширения экрана для ipad
    if UIDevice.current.userInterfaceIdiom == .pad {
      self.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
      self.navigationItem.leftItemsSupplementBackButton = false
    }
    //Операции
    createCell = CreateCollectionCell(collectionVC: self)
    selectCell = SelectCollectionCell(controller: self)
    //Обработка нажатия на ячейку
    tapToCell = UITapGestureRecognizer(target: self, action: #selector(pressOnCell(tap:)))
    collectionView.addGestureRecognizer(tapToCell)
    //view для выпадающего списка
    popoverView = optionView
    popoverPreView = preview
    //Создание заполнение таблицы
    _ = CreateEmptyGrid(controller: self)
    let operation = GetSquares(controller: self)
    operation.load(idPlan: selectedPlan.uid)
    //Установка зума
    zoom = ZoomSquare(controller: self)
    zoom.getSize()
    (collectionView.collectionViewLayout as! CustomCollectionViewLayout).delegate = self
    
  }
  
  //MARK: - Tap Gesture recognizer
  
  func pressOnCell(tap: UITapGestureRecognizer) {
    let location = tapToCell.location(in: collectionView)
    if let indexPath = collectionView.indexPathForItem(at: location) {
      selectCell?.tapOnCell(indexPath: indexPath, popoverPoint:tapToCell.location(in: view))
    }
  }
 
  //MARK: - Actions
  
  @IBAction func openSquare(_ sender: Any) {
    if let indexPath = selectedIndexPath {
      if squares[indexPath.section][indexPath.row] is Square {
        selectedSquare = (squares[indexPath.section][indexPath.row] as! Square)
      }
      else {
        selectedSquare = nil
      }
      selectCell?.popover.dismiss()
      performSegue(withIdentifier: "gotoUpdateSquare", sender: nil)
    }
  }
  
  @IBAction func copySquare(_ sender: Any) {
    if let indexPath = selectedIndexPath {
      if squares[indexPath.section][indexPath.row] is Square {
        selectedSquare = (squares[indexPath.section][indexPath.row] as! Square)
        operationOnCell = "copy"
        DDLogInfo("* Square is copied!")
      }
      else {
        selectedSquare = nil
        self.alert(withTitle: "Warning", andText: "Can not be copied empty object!")
        DDLogWarn("* Square not found!")
      }
      selectCell?.popover.dismiss()
    }
  }
  
  @IBAction func cutSquare(_ sender: Any) {
    if let indexPath = selectedIndexPath {
      if squares[indexPath.section][indexPath.row] is Square {
        selectedSquare = (squares[indexPath.section][indexPath.row] as! Square)
        operationOnCell = "cut"
        DDLogInfo("* Square is cut!")
      }
      else {
        selectedSquare = nil
        self.alert(withTitle: "Warning", andText: "Can not be cut empty object!")
        DDLogWarn("* Square not found!")
      }
      selectCell?.popover.dismiss()
    }
  }
  
  @IBAction func pasteSquare(_ sender: Any) {
    guard let indexPath = selectedIndexPath,
      let square = selectedSquare,
      let operation = operationOnCell
      else {
        selectCell?.popover.dismiss()
        self.alert(withTitle: "Error", andText: "Copy/Paste element not found!")
        DDLogWarn("* PasteSquare failed. IndexPath or square or operation is nil!")
        return
    }
    
    selectCell?.popover.dismiss()
    self.moveSquare(operation: operation, fromIndex: IndexPath.init(row: square.posY, section: square.posX), toIndex: indexPath)
  }
  
  @IBAction func deleteSquare(_ sender: Any) {
    if let indexPath = selectedIndexPath {
      selectCell?.popover.dismiss()
      squares[indexPath.section][indexPath.row] = ""
      collectionView.reloadData()
    }
  }
  
  @IBAction func confirmSquare(_ sender: Any) {
    
    if let indexPath = selectedIndexPath {
      let popover = PopoverOperations(controller: self)
      popover.confirm(indexPath: indexPath, editMode: isEditing, idPlan: selectedPlan.uid)
    }
  }
  
  @IBAction func zoomStep(_ sender: UIStepper) {
    //remove selected line
    if let selected = selectCell?.selectedLineView {
      selected.removeFromSuperview()
    }
    
    let size = sender.value
    zoom.setSize(size)
    setSquare(size: size)
    
  }
  
  @IBAction func editButton(_ sender: Any) {
    if isEditing {
      isEditing = false
      (collectionView.collectionViewLayout as! CustomCollectionViewLayout).isEditing = false
      editButton.title = "editMode"
      
      let operation = SaveProgress(controller: self)
      operation.save(objects: squares, plan: selectedPlan.uid, hNames: headerNames)
    }
    else {
      isEditing = true
      (collectionView.collectionViewLayout as! CustomCollectionViewLayout).isEditing = true
      editButton.title = "Save"
    }
    collectionView.reloadData()
  }
  
  //MARK: - Segues
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "gotoUpdateSquare" {
      let controller = segue.destination as! UpdateSquareVC
      controller.collectionVC = self
      controller.isEditing = isEditing
      if let selected = selectedSquare {
        controller.selectedSquare = selected
      }
      if let index = selectedIndexPath {
        controller.posX = index.section
        controller.posY = index.row
      }
    }
  }
  
  //MARK: - Other
  
  func setSquare(size: Double) {
    zoomStep.value = size
    (collectionView.collectionViewLayout as! CustomCollectionViewLayout).itemSize = CGSize(width: size, height: size/2)
    reloadLayout()
  }
  
  func reloadLayout() {
    (collectionView.collectionViewLayout as! CustomCollectionViewLayout).itemAttributes.removeAll()
    collectionView.collectionViewLayout.invalidateLayout()
  }
  
  func reloadCollection() {
    collectionView.reloadData()
  }
  
  func startSpinner() {
    spinner.isHidden = false
    spinner.startAnimating()
  }
  
  func stopSpinner() {
    spinner.stopAnimating()
  }
}

//MARK: - CollectionView Delegate

extension CollectionVC: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return squares.count
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return squares[section].count
  }
  
  func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    return createCell!.cellOn(collectionView: collectionView, withIndex: indexPath)
  }
}

//MARK: - Layout Delegate

extension CollectionVC: LayoutDelegate {
  func moveSquare(operation: String, fromIndex: IndexPath, toIndex: IndexPath) {
    if squares[toIndex.section][toIndex.row] is Square {
      var nearestEmptyCellRow = toIndex.row
      for i in toIndex.row+1...squares[toIndex.section].count-1 {
        if !(squares[toIndex.section][i] is Square) {
          nearestEmptyCellRow = i
          break
        }
      }
      var tmp:Square? = nil
      for i in toIndex.row...nearestEmptyCellRow-1 {
        
        var obj = squares[toIndex.section][i] as! Square
        if tmp != nil {
          obj = tmp!
        }
        tmp = squares[toIndex.section][i+1] as? Square
        obj.updatePosition(x: toIndex.section, y: toIndex.row+1)
        
        squares[toIndex.section][i+1] = obj
      }
      
    }
    guard var tmpSquare = (squares[fromIndex.section][fromIndex.row] as? Square) else {
      DDLogError("* empry square")
      return
    }
    
    if operation == "cut" {
      tmpSquare.updatePosition(x: toIndex.section, y: toIndex.row)
      squares[toIndex.section][toIndex.row] = tmpSquare
      squares[fromIndex.section][fromIndex.row] = ""
      selectedSquare = nil
    }
    else if operation == "copy" {
      squares[toIndex.section][toIndex.row] = Square(posX: toIndex.section, posY: toIndex.row, title: tmpSquare.title, content: tmpSquare.content,dopContent: tmpSquare.dopContent, color: tmpSquare.color)
    }
    
    collectionView.reloadData()
  }
}

