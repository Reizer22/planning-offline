//
//  SplitVC.swift
//  Planning
//
//  Created by admin on 26.08.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class SplitVC: UISplitViewController,UISplitViewControllerDelegate {
  
  override func viewDidLoad() {
    self.delegate = self
    self.preferredDisplayMode = .allVisible
    self.preferredPrimaryColumnWidthFraction = 0.3
  }
  
  func splitViewController(
    _ splitViewController: UISplitViewController,
    collapseSecondary secondaryViewController: UIViewController,
    onto primaryViewController: UIViewController) -> Bool {
    return true
  }
  
}

