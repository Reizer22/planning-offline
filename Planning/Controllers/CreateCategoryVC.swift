//
//  CreateCategoryVC.swift
//  Planning
//
//  Created by admin on 04.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit


protocol CreateCategoryDelegate {
  func create(_ plan: Plan)
  func update(_ plan: Plan)
}

class CreateCategoryVC: UIViewController {
  
  @IBOutlet weak var titleLabel: UILabel!
  var activeTextField: UITextField?
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var iconsCollection: UICollectionView!
  @IBOutlet weak var datePicker: UIDatePicker!
  @IBOutlet weak var nameTextField: UITextField!
  
  @IBOutlet weak var editPermissTextfield: UITextField!
  @IBOutlet weak var confirmPermissTextfield: UITextField!
  var iconNames: [String] {
    return ["Drill.png", "Hammer.png", "Hatchet.png", "PaintBrush.png", "PaintBucket.png", "Pliers.png","RollerBrush.png","Saw.png","Screw.png","Screwdriver.png","Spade.png","TapeMeasure.png","Toolbox.png","WallpaperRoll.png","waterhouse.png","Wateringcan.png","Wheelbarrow.png"]
  }
  var selectedIndex:IndexPath? = nil
  var delegate: CreateCategoryDelegate!
  var titleVC: String = "Создать"
  var currentPlan: Plan?
  
  //MARK: - Load
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //События клавиатуры
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    //Установки полей для категории
    titleLabel.text = titleVC
    if let plan = currentPlan {
      nameTextField.text = plan.name
      datePicker.date = plan.startDate
      for i in 0..<iconNames.count {
        if iconNames[i] == plan.iconName {
          selectedIndex = IndexPath(row: i, section: 0)
          iconsCollection.selectItem(at: selectedIndex, animated: true, scrollPosition: .centeredHorizontally)
          break
        }
      }
    }
  }
  
  //MARK: - Notification Keyboard
  
  func keyboardWillShow(notification: NSNotification) {
    if let keyboardSize = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {
      
      var aRect = self.view.frame;
      var contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
      if aRect.size.height == 667 {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, 100, 0.0);
      }
      scrollView.contentInset = contentInsets
      scrollView.scrollIndicatorInsets = contentInsets
      
      aRect.size.height -= keyboardSize.height
      
      if let active = activeTextField {
        if !aRect.contains(active.frame.origin) {
          scrollView.scrollRectToVisible(active.frame, animated: true)
        }
      }
      
    }
  }
  
  func keyboardWillHide(notification: NSNotification) {
    let contentInsets = UIEdgeInsets.zero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    scrollView.scrollRectToVisible(titleLabel.frame, animated: true)
  }
  
  //MARK: - Actions

  @IBAction func cancelButton(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func saveButton(_ sender: Any) {
    if nameTextField.text == "" {
      self.alert(withTitle: "Error", andText: "Name is empty!")
    }
    else {
      var iconName:String = "default.png"
      if selectedIndex != nil {
        iconName = iconNames[selectedIndex!.row]
      }
      
      editPermissTextfield.text == "" ? (editPermissTextfield.text = "-") : ()
      confirmPermissTextfield.text == "" ? (confirmPermissTextfield.text = "-") : ()
      
      let plan = Plan(iconName: iconName, name: nameTextField.text!, startDate: datePicker.date)
      if titleVC == "Создать" {
        delegate.create(plan)
      }
      else {
        delegate.update(plan)
      }
      
      
      self.dismiss(animated: true, completion: nil)
    }
  }
}

//MARK: - collectionView Delegate

extension CreateCategoryVC: UICollectionViewDelegate, UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return iconNames.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "iconsCell", for: indexPath) as! CustomCollectionViewCell
    
    if selectedIndex == indexPath {
      cell.layer.borderWidth = 2.0
      cell.layer.borderColor = UIColor.gray.cgColor
    }
    else {
      cell.layer.borderWidth = 0.0
    }
    cell.iconImage.image = UIImage(named: iconNames[indexPath.row])
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    print(indexPath.row)
    let cell = collectionView.cellForItem(at: indexPath)
    cell?.layer.borderWidth = 2.0
    cell?.layer.borderColor = UIColor.gray.cgColor
    
    selectedIndex = indexPath
  }
  
  func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath)
    cell?.layer.borderColor = .none
    cell?.layer.borderWidth = 0.0
  }
}

//MARK: - textField Delegate

extension CreateCategoryVC: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    nameTextField.resignFirstResponder()
    editPermissTextfield.resignFirstResponder()
    confirmPermissTextfield.resignFirstResponder()
    return true
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    activeTextField = textField
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    activeTextField = nil
  }
}

