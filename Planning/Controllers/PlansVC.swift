//
//  PlansVC.swift
//  Planning
//
//  Created by admin on 21.06.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwipeCellKit
import CocoaLumberjack

class PlansVC: UITableViewController {
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  var plans = [Plan]()
  var selectedIndexPath: IndexPath? = nil
  
  //MARK: - Load
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let plan = GetPlans(controller: self)
    plan.uploadListFromCoreData()
    self.navigationItem.leftBarButtonItem = self.editButtonItem

  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  //MARK: - tableView Delegate
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return plans.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell") as! CustomTableViewCell
    cell.delegate = self
    cell.titleCategory.text = plans[indexPath.row].name
    cell.imageCategory.image = UIImage(named: plans[indexPath.row].iconName)
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
    if self.tableView.isEditing {
      return UITableViewCellEditingStyle.delete
    }
    return UITableViewCellEditingStyle.none
  }
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      let operation = DeletePlan(controller: self)
      operation.delete(atRow: indexPath.row)
      self.tableView.deleteRows(at: [indexPath], with: .automatic)
    }
  }
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectedIndexPath = indexPath
    performSegue(withIdentifier: "gotoCollection", sender: nil)
  }
  
  override func setEditing(_ editing: Bool, animated: Bool) {
    super.setEditing(editing, animated: animated)
    self.tableView.setEditing(editing, animated: animated)
  }
  
  //MARK: - Actions
  
  @IBAction func addButton(_ sender: Any) {
    performSegue(withIdentifier: "gotoCreateCategory", sender: nil)
  }
  
  @IBAction func editButton(_ sender: Any) {
    if isEditing {
      setEditing(false, animated: true)
    }
    else {
      setEditing(true, animated: true)
    }
  }
  
  func startSpinner() {
    spinner.isHidden = false
    spinner.startAnimating()
  }
  
  func stopSpinner() {
    spinner.stopAnimating()
  }
  
  //MARK: - Segues
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "gotoCreateCategory" || segue.identifier == "gotoEditCategory" {
      let category = segue.destination as! CreateCategoryVC
      category.delegate = self
      
      if segue.identifier == "gotoEditCategory" {
        category.titleVC = "Редактировать"
        if let indexPath = selectedIndexPath {
          category.currentPlan = plans[indexPath.row]
        }
      }
    }
    
    if segue.identifier == "gotoCollection" {
      let collection = segue.destination as! CollectionVC
      collection.delegate = self
      if let indexPath = selectedIndexPath {
        collection.title = plans[indexPath.row].name
        collection.selectedPlan = plans[indexPath.row]
      }
      else {
        DDLogError("* Segue gotoCollection: selectedIndexPath is nil!")
      }
    }
  }
}

//MARK: - SwipeTableViewCell Delegate

extension PlansVC: SwipeTableViewCellDelegate {
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
    guard orientation == .right else {
      return nil
    }
    let deleteAction = SwipeAction(style: .destructive, title: "Удалить") { action, indexPath in
      
      let operation = DeletePlan(controller: self)
      operation.delete(atRow: indexPath.row)
      
      self.tableView.beginUpdates()
      self.tableView.deleteRows(at: [indexPath], with: .automatic)
      action.fulfill(with: .delete)
      self.tableView.endUpdates()
      
    }
    let editAction = SwipeAction(style: .default, title: "Редактировать") { action, indexPath in
      
      self.selectedIndexPath = indexPath
      self.performSegue(withIdentifier: "gotoEditCategory", sender: nil)
      
    }
    deleteAction.hidesWhenSelected = true
    editAction.hidesWhenSelected = true
    
    return [deleteAction, editAction]
  }
}

//MARK: - CreateCategory Delegate

extension PlansVC: CreateCategoryDelegate {
  func create(_ plan: Plan) {
    let operation = AddPlan(controller: self)
    operation.insert(plan)
  }
  
  func update(_ plan: Plan) {
    
    if let indexPath = selectedIndexPath {
      let operation = UpdatePlan(controller: self)
      operation.update(atRow: indexPath.row, plan: plan)
    }
    else {
      DDLogError("* Index is nil")
    }
  }
}

//MARK: - CollectionVC Delegate

extension PlansVC: CollectionVCDelegate {
  func updateSelected(_ plan: Plan) {
    for i in 0...plans.count-1 {
      if plans[i].uid == plan.uid {
        plans[i] = plan
        break
      }
    }
  }
}


