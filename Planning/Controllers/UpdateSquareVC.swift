//
//  UpdateSquareVC.swift
//  Planning
//
//  Created by admin on 21.06.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CocoaLumberjack



class UpdateSquareVC: UIViewController,UIGestureRecognizerDelegate {
  
  enum Edit {
    case content
    case dopContent
  }
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  var collectionVC: CollectionVC!
  var selectedSquare: Square?
  var posX:Int!
  var posY:Int!
  
  
  let colors = [#colorLiteral(red: 1, green: 0.3005838394, blue: 0.2565174997, alpha: 1), #colorLiteral(red: 0.6717393994, green: 0.08416283876, blue: 0, alpha: 1), #colorLiteral(red: 1, green: 0.4863265157, blue: 0, alpha: 1), #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1), #colorLiteral(red: 0.8320295215, green: 0.9826709628, blue: 0, alpha: 1), #colorLiteral(red: 0.65173316, green: 0.9811168313, blue: 0.2557956874, alpha: 1), #colorLiteral(red: 0.4497856498, green: 0.9784941077, blue: 0, alpha: 1), #colorLiteral(red: 0.2176683843, green: 0.8194433451, blue: 0.2584097683, alpha: 1), #colorLiteral(red: 0.461088419, green: 0.8273150325, blue: 0.6642131209, alpha: 1), #colorLiteral(red: 0.1927526891, green: 0.9871093631, blue: 0.8377645016, alpha: 1), #colorLiteral(red: 0.1941846311, green: 0.991915524, blue: 1, alpha: 1), #colorLiteral(red: 0, green: 0.6730770469, blue: 1, alpha: 1), #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1), #colorLiteral(red: 0, green: 0.2834656835, blue: 0.6651299596, alpha: 1), #colorLiteral(red: 0.4826515317, green: 0.2100505233, blue: 1, alpha: 1), #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1), #colorLiteral(red: 1, green: 0.1568113565, blue: 0.2567096651, alpha: 1), #colorLiteral(red: 0.8801901937, green: 0.3436613679, blue: 0.2941911221, alpha: 1), #colorLiteral(red: 0.807002008, green: 0.2590857148, blue: 0.4034079909, alpha: 1), #colorLiteral(red: 0.5575397611, green: 0.2693634629, blue: 0.6715298891, alpha: 1), #colorLiteral(red: 0.3726176023, green: 0.2959010899, blue: 0.6930241585, alpha: 1), #colorLiteral(red: 0.2532154918, green: 0.352729857, blue: 0.6847487688, alpha: 1), #colorLiteral(red: 0.27835989, green: 0.6047654748, blue: 0.9221691489, alpha: 1), #colorLiteral(red: 0.2911589146, green: 0.6696680784, blue: 0.9279757142, alpha: 1), #colorLiteral(red: 0.3335199952, green: 0.7267344594, blue: 0.8125833869, alpha: 1), #colorLiteral(red: 0.2637858391, green: 0.5718953609, blue: 0.5271097422, alpha: 1), #colorLiteral(red: 0.4113362134, green: 0.6588250995, blue: 0.3461188972, alpha: 1), #colorLiteral(red: 0.597264111, green: 0.7384731174, blue: 0.3502138257, alpha: 1), #colorLiteral(red: 0.8186559081, green: 0.8392770886, blue: 0.3452027738, alpha: 1), #colorLiteral(red: 0.9893675447, green: 0.903722465, blue: 0.3823662996, alpha: 1), #colorLiteral(red: 0.9634999633, green: 0.752433002, blue: 0.2878698409, alpha: 1), #colorLiteral(red: 0.9437018037, green: 0.609736383, blue: 0.2575133443, alpha: 1), #colorLiteral(red: 0.9233137965, green: 0.4034566283, blue: 0.2648251057, alpha: 1), #colorLiteral(red: 0.452852726, green: 0.3394546509, blue: 0.2952045202, alpha: 1), #colorLiteral(red: 0.6195520163, green: 0.619643569, blue: 0.6195320487, alpha: 1), #colorLiteral(red: 0.3996780217, green: 0.4887441397, blue: 0.5371351242, alpha: 1)]
  var selectedColor = UIColor.white
  var selectedIndex:IndexPath? = nil
  
  @IBOutlet weak var collectionViewColorPicker: UICollectionView!
  @IBOutlet weak var editDopContentTitle: UIButton!
  @IBOutlet weak var editContentTitle: UIButton!
  @IBOutlet weak var dopContentLabel: UILabel!
  @IBOutlet weak var contentLabel: UILabel!
  @IBOutlet weak var titleSquare: UITextField!
  @IBOutlet weak var contentSquare: UITextView!
  @IBOutlet weak var dopContentSquare: UITextView!
  @IBOutlet weak var saveButton: UIButton!
  @IBOutlet weak var confirmSwitcher: UISwitch!
  
  //MARK: - Load
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //Установки для реж. просмотра
    if !isEditing {
      confirmSwitcher.isEnabled = false
      titleSquare.isEnabled = false
      contentSquare.isUserInteractionEnabled = false
      dopContentSquare.isUserInteractionEnabled = false
      saveButton.isEnabled = false
      editContentTitle.isHidden = true
      editDopContentTitle.isHidden = true
    }
    //Установки для объекта
    if let selected = selectedSquare {
      
      titleSquare.text = selected.title
      contentSquare.text = selected.content
      dopContentSquare.text = selected.dopContent
      
      if (selectedSquare?.confirm)! {
        confirmSwitcher.setOn(true, animated: true)
      }
      
      for i in 0..<colors.count {
        // print("* \(colors[i].toHexString) - \(selected.color.toHexString)")
        if colors[i].toHexString == selected.color.toHexString {
          selectedIndex = IndexPath(row: i, section: 0)
          selectedColor = selected.color
          collectionViewColorPicker.selectItem(at: selectedIndex, animated: true, scrollPosition: .centeredHorizontally)
          break
        }
      }
    }
    else {
      DDLogInfo("* UpdateSquareVC: selectedSquare is nil.")
    }
    //Установки заголовков для доп. информации
    if (collectionVC.selectedPlan.contentTitle != nil) && collectionVC.selectedPlan.contentTitle != "" {
      contentLabel.text = collectionVC.selectedPlan.contentTitle!
    }
    if (collectionVC.selectedPlan.dopContentTitle != nil) && collectionVC.selectedPlan.dopContentTitle != "" {
      dopContentLabel.text = collectionVC.selectedPlan.dopContentTitle!
    }
  }
  
  //MARK: - Actions
  
  @IBAction func confirmSwitcher(_ sender: Any) {
    
  }
  
  @IBAction func cancelButton(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func saveButton(_ sender: Any) {
    
    let newSquare = Square(posX: posX, posY: posY, title: titleSquare.text!, content: contentSquare.text!,dopContent: dopContentSquare.text!, color: selectedColor, confirm: confirmSwitcher.isOn == true)
    _ = UpdateSquare(square: newSquare, andReload: collectionVC)
    
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func editContentTitle(_ sender: Any) {
    newNameContentTitle(Edit.content)
  }
  @IBAction func editDopContentTitle(_ sender: Any) {
    newNameContentTitle(Edit.dopContent)
  }
  
  func newNameContentTitle(_ textView: Edit) {
    let alert = UIAlertController(title: "Rename cell", message: "Enter text", preferredStyle: .alert)
    alert.addTextField { (textField) in
      if textView == .content {
        textField.text = self.contentLabel.text
      }
      else if textView == .dopContent {
        textField.text = self.dopContentLabel.text
      }
    }
    alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { [weak alert] (_) in
      let textField = alert?.textFields![0]
      
      let operation = UpdateContentTitles(self.collectionVC)
      if textView == .content {
        self.contentLabel.text = textField?.text
        operation.update(contentTitle: (textField?.text)!)
      }
      else if textView == .dopContent {
        self.dopContentLabel.text = textField?.text
        operation.update(dopContentTitle: (textField?.text)!)
      }
    }))
    
    self.present(alert, animated: true, completion: nil)
  }
  
}

//MARK: - collectionView Delegate

extension UpdateSquareVC: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return colors.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorsCell", for: indexPath)
    cell.backgroundColor = colors[indexPath.row]
    
    if selectedIndex == indexPath {
      cell.layer.borderWidth = 3.0
      cell.layer.borderColor = UIColor.black.cgColor
    }
    else {
      cell.layer.borderWidth = 0.0
    }
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    print(indexPath.row)
    let cell = collectionView.cellForItem(at: indexPath)
    cell?.layer.borderWidth = 3.0
    cell?.layer.borderColor = UIColor.black.cgColor
    
    selectedIndex = indexPath
    selectedColor = colors[indexPath.row]
  }
  
  func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath)
    cell?.layer.borderColor = .none
    cell?.layer.borderWidth = 0.0
  }
}

//MARK: - textField Delegate

extension UpdateSquareVC: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    titleSquare.resignFirstResponder()
    return true
  }
}

//MARK: - textView Delegate

extension UpdateSquareVC: UITextViewDelegate {
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    if(text == "\n") {
      contentSquare.resignFirstResponder()
      dopContentSquare.resignFirstResponder()
      return false
    }
    return true
  }
}

