//
//  HorizontalCollectionViewCell.swift
//  Planning
//
//  Created by admin on 26.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class HorizontalCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var horizontalLabel: UILabel!
    @IBOutlet weak var confirmImageView: UIImageView!
}
