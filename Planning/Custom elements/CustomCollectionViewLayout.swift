//
//  CustomCollectionViewLayout.swift
//  Planning
//
//  Created by admin on 26.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CocoaLumberjack

protocol LayoutDelegate {
  func moveSquare(operation: String, fromIndex: IndexPath, toIndex: IndexPath)
}

class CustomCollectionViewLayout: UICollectionViewLayout {
  
  var delegate: LayoutDelegate?
  
  var itemSize = CGSize(width: 100, height: 50)
  var itemAttributes = [[UICollectionViewLayoutAttributes]]()
  var contentSize : CGSize?
  
  var tapToCell:UITapGestureRecognizer!
  var longPress: UILongPressGestureRecognizer!
  var originalIndexPath: IndexPath?
  var draggingIndexPath: IndexPath?
  var draggingView: UIView?
  var dragOffset = CGPoint.zero
  var movedToIndexPath: IndexPath? = nil
  
  var isEditing:Bool = false
  
  //MARK: - Tap Gesture Recognizer
  
  func installGestureRecognizer() {
    //Обработчик долгого нажатия на коллекцию
    if longPress == nil {
      longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPress:)))
      longPress.minimumPressDuration = 0.2
      collectionView?.addGestureRecognizer(longPress)
    }
  }
  
  func handleLongPress(longPress: UILongPressGestureRecognizer) {
    //Для режима редактирования
    if isEditing {
      let location = longPress.location(in: collectionView!)
      print("x: \(location.x) y: \(location.y)")
      //Обработка состояний
      switch longPress.state {
      case .began: startDragAtLocation(location: location)
      case .changed: updateDragAtLocation(location: location)
      case .ended: endDragAtLocation(location: location)
      default:
        break
      }
    }
  }
  
  func startDragAtLocation(location: CGPoint) {
    guard let cv = collectionView else { return }
    guard let indexPath = cv.indexPathForItem(at: location) else { return }
    guard let cell = cv.cellForItem(at: indexPath) else { return }
    
    originalIndexPath = indexPath
    draggingIndexPath = indexPath
    //создание view выделенной ячейки
    draggingView = cell.snapshotView(afterScreenUpdates: true)
    draggingView!.frame = cell.frame
    dragOffset = CGPoint(x: draggingView!.center.x - location.x, y: draggingView!.center.y - location.y)
    
    //Создание view горизонтального заголовка
    if indexPath.row == 0 {
      var k:Int = 0
      for i in 1..<cv.numberOfItems(inSection: 1) {
        guard let viewCell = cv.cellForItem(at: IndexPath.init(row: indexPath.row+i, section: indexPath.section))
        else {
          continue
        }
        if k == 0 {
          k+=1
          if i != 1 {
            continue
          }
        }
        let tmpView = viewCell.snapshotView(afterScreenUpdates: true)!
        tmpView.alpha = 0.8
        tmpView.frame = CGRect(x: CGFloat(Int(cell.frame.width)*k), y: 0, width: cell.frame.width, height: cell.frame.height)
        draggingView?.addSubview(tmpView)
        k+=1
      }
      dragOffset = CGPoint(x: draggingView!.center.x, y: draggingView!.center.y - location.y)
    }
    //создание view вертикального заголовка
    else if indexPath.section == 0 {
      var k:Int = 0
      for i in 1..<cv.numberOfSections {
        
        guard let viewCell = cv.cellForItem(at: IndexPath.init(row: indexPath.row, section: indexPath.section+i)) else {
          continue
        }
        if k == 0 {
          k+=1
          if i != 1 {
            continue
          }
        }
        let tmpView = viewCell.snapshotView(afterScreenUpdates: true)!
        tmpView.alpha = 0.8
        tmpView.frame = CGRect(x: 0, y: CGFloat(Int(cell.frame.height)*k), width: cell.frame.width, height: cell.frame.height)
        draggingView?.addSubview(tmpView)
        k+=1
      }
      dragOffset = CGPoint(x: draggingView!.center.x - location.x, y: draggingView!.center.y )
    }
    //Добавление на коллекцию
    cv.addSubview(draggingView!)
    
    draggingView?.layer.shadowPath = UIBezierPath(rect: draggingView!.bounds).cgPath
    draggingView?.layer.shadowColor = UIColor.black.cgColor
    draggingView?.layer.shadowOpacity = 0.8
    draggingView?.layer.shadowRadius = 10
    
    invalidateLayout()
    //Анимация выделения
    UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
      self.draggingView?.alpha = 0.95
      self.draggingView?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
    }, completion: nil)
  }
  
  func updateDragAtLocation(location: CGPoint) {
    guard let view = draggingView,
          let cv = collectionView,
          let indexPath = originalIndexPath
    else {
      return
    }
    //Новое положение
    if indexPath.row == 0 {
      view.center = CGPoint(x: dragOffset.x, y: location.y + dragOffset.y)
    }
    else if indexPath.section == 0 {
      view.center = CGPoint(x: location.x + dragOffset.x, y: dragOffset.y)
    }
    else {
      view.center = CGPoint(x: location.x + dragOffset.x,y: location.y + dragOffset.y)
    }
    
    if let newIndexPath = cv.indexPathForItem(at: location) {
      draggingIndexPath = newIndexPath
    }
  }
  
  func endDragAtLocation(location: CGPoint) {
    guard let dragView = draggingView,
          let indexPath = draggingIndexPath,
          let cv = collectionView,
          let datasource = cv.dataSource
    else {
      return
    }
    
    let targetCenter = dragView.center
    //Анимация окончания перетаскивания
    UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
      dragView.center = targetCenter
      dragView.transform = CGAffineTransform.identity
    }) { (completed) in
      
      
      if indexPath != self.originalIndexPath! {
        if #available(iOS 9.0, *) {
          datasource.collectionView!(cv, moveItemAt: self.originalIndexPath!, to: indexPath)
        } else {
          DDLogWarn("iOS earlier 9")
        }
      }
      //Удаление перетаскиваемой view
      dragView.removeFromSuperview()
      self.draggingIndexPath = nil
      self.draggingView = nil
      self.invalidateLayout()
      //Запись новых координат
      self.delegate?.moveSquare(operation: "cut", fromIndex: self.originalIndexPath!, toIndex: indexPath)
      
      //            let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath.init(row: self.itemAttributes[indexPath.section].count, section: indexPath.section))
      //
      //            attributes.frame = CGRect(x: self.itemAttributes[indexPath.section].last!.frame.origin.x+122,
      //                                      y: self.itemAttributes[indexPath.section].last!.frame.origin.y,
      //                                      width: self.itemAttributes[indexPath.section].last!.frame.width,
      //                                      height: self.itemAttributes[indexPath.section].last!.frame.height).integral
      //
      //            self.itemAttributes[indexPath.section].append(attributes)
      //
      //            self.itemAttributes[(self.originalIndexPath?.section)!].removeLast()
      
      self.movedToIndexPath = indexPath
    }
    
  }
  
  //MARK: - Load
  
  override func prepare() {
    
    installGestureRecognizer()
    //Таблица не построена
    if collectionView?.numberOfSections == 0 {
      return
    }
    //Таблица существует
    if (itemAttributes.count > 0) {
      for section in 0..<collectionView!.numberOfSections {
        let numberOfItems = collectionView!.numberOfItems(inSection: section)
        for index in 0..<numberOfItems {
          if section != 0 && index != 0 {
            continue
          }
          let attributes = layoutAttributesForItem(at: IndexPath(item: index, section: section))
          if section == 0 {
            var frame = attributes?.frame
            frame?.origin.y = collectionView!.contentOffset.y
            attributes?.frame = frame!
          }
          
          if index == 0 {
            var frame = attributes?.frame
            frame?.origin.x = collectionView!.contentOffset.x
            attributes?.frame = frame!
          }
        }
      }
      return
    }
    //Построение layout таблицы и запись в массив
    var column = 0
    var xOffset : CGFloat = 0
    var yOffset : CGFloat = 0
    var contentWidth : CGFloat = 0
    var contentHeight : CGFloat = 0
    
    for section in 0..<collectionView!.numberOfSections {
      var sectionAttributes = [UICollectionViewLayoutAttributes]()
      
      for index in 0..<collectionView!.numberOfItems(inSection: section) {
        
        let indexPath = IndexPath(item: index, section: section)
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height).integral
        
        if section == 0 && index == 0 {
          attributes.zIndex = 1024;
        } else  if section == 0 || index == 0 {
          attributes.zIndex = 1023
        }
        
        if section == 0 {
          var frame = attributes.frame
          frame.origin.y = self.collectionView!.contentOffset.y
          attributes.frame = frame
        }
        if index == 0 {
          var frame = attributes.frame
          frame.origin.x = self.collectionView!.contentOffset.x
          attributes.frame = frame
        }
        sectionAttributes.append(attributes)
        
        xOffset += itemSize.width+2
        column += 1
        
        if column == collectionView?.numberOfItems(inSection: section) {
          if xOffset > contentWidth {
            contentWidth = xOffset
          }
          
          column = 0
          xOffset = 0
          yOffset += itemSize.height+2
        }
      }
      itemAttributes.append(sectionAttributes)
    }
    
    let lastAttributes = itemAttributes.last?[(itemAttributes.last?.count)!-1]
    contentHeight = (lastAttributes?.frame.origin.y)! + (lastAttributes?.frame.size.height)!
    contentSize = CGSize(width: contentWidth, height: contentHeight)
    
  }
  
  //MARK: - CollectionView Layout
  
  override var collectionViewContentSize : CGSize {
    return contentSize!
  }
  
  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    //Возврат layout из готового массива
    return itemAttributes[indexPath.section][indexPath.row]
  }
  
  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return true
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var attributes = [UICollectionViewLayoutAttributes]()
    
    for section in self.itemAttributes {
      
      attributes.append(contentsOf: section)
      
    }
    
    return attributes
  }
  
}

