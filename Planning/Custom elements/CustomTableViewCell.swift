//
//  CustomTableViewCell.swift
//  Planning
//
//  Created by admin on 04.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SwipeCellKit

class CustomTableViewCell: SwipeTableViewCell {
  
  @IBOutlet weak var imageCategory: UIImageView!
  @IBOutlet weak var titleCategory: UILabel!
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}

