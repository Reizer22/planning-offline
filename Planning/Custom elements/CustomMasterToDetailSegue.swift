//
//  CustomMasterToDetailSegue.swift
//  Planning
//
//  Created by admin on 10.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class CustomMasterToDetailSegue: UIStoryboardSegue {
  
  override func perform() {
    let source = self.source
    let destination = self.destination
    //Открытие master controller  в detail на  iphone
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
      UIView.transition(with: (source.navigationController?.view)!, duration: 0.2, options: UIViewAnimationOptions(rawValue: 0), animations: {source.navigationController?.pushViewController(destination, animated: true)}, completion: nil)
    }
    else {
      let detailNav = source.splitViewController?.viewControllers[1] as! UINavigationController
      detailNav.pushViewController(destination, animated: true)
    }
  }
  
}

