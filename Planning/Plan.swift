//
//  Plan.swift
//  Planning
//
//  Created by admin on 09.08.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

struct Plan {
  var uid: String
  var iconName: String
  var name: String
  var startDate: Date
  var contentTitle: String?
  var dopContentTitle: String?
  
  init(uid: String = UUID().uuidString, iconName: String, name: String, startDate: Date, contentTitle: String? = nil, dopContentTitle:String? = nil) {
    self.uid = uid
    self.iconName = iconName
    self.name = name
    self.startDate = startDate
    self.contentTitle = contentTitle
    self.dopContentTitle = dopContentTitle
  }
  
  mutating func update(name: String, startDate: Date, iconName: String) {
    self.iconName = iconName
    self.name = name
    self.startDate = startDate
  }
  
  mutating func insert(_ uid: String) {
    self.uid = uid
  }
  
  mutating func setContentTitle(title: String) {
    self.contentTitle = title
  }
  
  mutating func setDopContentTitle(title: String) {
    self.dopContentTitle = title
  }
}

