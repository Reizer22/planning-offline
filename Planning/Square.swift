//
//  Square.swift
//  Planning
//
//  Created by admin on 21.06.17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit


struct Square {
  var posX: Int
  var posY: Int
  var title: String
  var content: String
  var dopContent: String
  var color: UIColor
  var confirm: Bool
  
  init(posX: Int, posY: Int, title:String = "", content: String = "", dopContent: String = "", color: UIColor = UIColor.white, confirm: Bool = false) {
    self.posX = posX
    self.posY = posY
    self.title = title
    self.content = content
    self.dopContent = dopContent
    self.color = color
    self.confirm = confirm
  }
  
  mutating func updatePosition(x: Int, y:Int) {
    posX = x
    posY = y
  }
  
  mutating func setConfirm(_ conf: Bool) {
    confirm = conf
  }
}

