//
//  UIViewController+Activity.swift
//  Planning
//
//  Created by admin on 04.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  func displayNavBarActivity() {
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    indicator.startAnimating()
    let item = UIBarButtonItem(customView: indicator)
    
    self.navigationItem.rightBarButtonItem = item
  }
  
  func dismissNavBarActivity() {
    self.navigationItem.rightBarButtonItem = nil
  }
}

