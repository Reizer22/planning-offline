//
//  Date+DaysBetweenDate.swift
//  Planning
//
//  Created by admin on 25.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation

extension Date {
  func daysBetweenDate(toDate: Date) -> Int {
    let components = Calendar.current.dateComponents([.day], from: self, to: toDate)
    return components.day ?? 0
  }
}

