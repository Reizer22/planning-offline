//
//  UIViewController+RemoveViews.swift
//  Planning
//
//  Created by admin on 25.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  func removeAnotherOpenViews() {
    var navArray = self.navigationController?.viewControllers
    if navArray?.count != 2 && UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
      var navArrayTmp = [UIViewController]()
      navArrayTmp.append((navArray?[0])!)
      navArrayTmp.append((navArray?[(navArray?.count)!-1])!)
      self.navigationController?.viewControllers = navArrayTmp
    }
  }
}

