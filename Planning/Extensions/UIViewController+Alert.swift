//
//  UIViewController+Alert.swift
//  Planning
//
//  Created by admin on 04.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  func alert(withTitle title: String, andText text:String){
    let alertController = UIAlertController(title: title, message: text, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alertController.addAction(okAction)
    self.present(alertController, animated: true, completion: nil)
  }
  
  
}

