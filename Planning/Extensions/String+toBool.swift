//
//  String+toBool.swift
//  Planning
//
//  Created by admin on 06.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit

extension String {
  func toBool() -> Bool? {
    switch self {
    case "True","true","yes","1":
      return true
    case "False","false","no","0":
      return false
    default:
      return nil
      
    }
  }
}

