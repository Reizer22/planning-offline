//
//  UpdateContentTitles.swift
//  Planning
//
//  Created by admin on 09.08.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class UpdateContentTitles: NSObject {
  
  let collectionVC: CollectionVC!
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  init(_ collectionVC: CollectionVC) {
    self.collectionVC = collectionVC
  }
  
  func update(contentTitle: String) {
    collectionVC.selectedPlan.setContentTitle(title: contentTitle)
    updateOnDatabase(title: contentTitle, type: "ContentTitle")
  }
  
  func update(dopContentTitle: String) {
    collectionVC.selectedPlan.setDopContentTitle(title: dopContentTitle)
    updateOnDatabase(title: dopContentTitle, type: "DopContentTitle")
  }
  
  func updateOnDatabase(title: String, type: String) {
    
    //self.collectionVC.delegate.updateSelected(self.collectionVC.selectedPlan)
    
  }
}

