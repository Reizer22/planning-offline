//
//  UpdatePlan.swift
//  Planning
//
//  Created by admin on 04.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class UpdatePlan: CacheOperation {
  
  func update(atRow row:Int, plan: Plan) {
    let plansVC = controller as! PlansVC
    self.updateCoreData(row, plan, plansVC)
    self.updateArray(row, plan, plansVC)
  }
  
  func updateArray(_ row:Int,_ plan: Plan,_ plansVC: PlansVC) {
    var newPlan = plansVC.plans[row]
    newPlan.update(name: plan.name, startDate: plan.startDate, iconName: plan.iconName)
    
    plansVC.plans[row] = newPlan
    
    plansVC.tableView.reloadData()
  }
  
  func updateCoreData(_ row:Int,_ plan: Plan,_ plansVC: PlansVC) {
    
    plansVC.startSpinner()
    self.objectContext?.performAndWait {
      
      let pred = NSPredicate(format: "uid == %@", plansVC.plans[row].uid.uppercased())
      let request: NSFetchRequest<PlanEntity> = PlanEntity.fetchRequest()
      request.predicate = pred
      do {
        if let planEntity = try self.objectContext?.fetch(request) {
          if planEntity.count > 0 {
            planEntity[0].setValuesForKeys(["name":plan.name, "iconName": plan.iconName, "StartDate":plan.startDate])
            do {
              try self.objectContext?.save()
            } catch {
              
            }
            DDLogInfo("* Update plan on CoreData complete!")
          }
          else {
            DDLogError("* UpdateCoreData. Plan is not found!")
          }
        }
      } catch {
        DDLogError("* UpdateCoreData. Error result")
      }
      plansVC.stopSpinner()
    }
  }
  
}

