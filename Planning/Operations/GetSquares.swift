//
//  GetSquares.swift
//  Planning
//
//  Created by admin on 22.06.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class GetSquares: CacheOperation {
  
  var uploadedSquares = [Square]()
  
  func load(idPlan:String) {
    let collectionVC = controller as! CollectionVC
    self.loadFromCoreData(idPlan: idPlan, collectionVC: collectionVC)
  }
  
  func loadFromCoreData(idPlan: String, collectionVC: CollectionVC) {
    collectionVC.startSpinner()
    let pred = NSPredicate(format: "idPlan == %@", idPlan.uppercased())
    //upload squares
    self.objectContext?.performAndWait {
      let request: NSFetchRequest<SquaresEntity> = SquaresEntity.fetchRequest()
      request.predicate = pred
      do {
        guard let result = try self.objectContext?.fetch(request) else {
          DDLogError("* Upload squares. Error result!")
          return
        }
        for cacheSquare in result {
          
          let square = Square(posX: Int(cacheSquare.posX), posY: Int(cacheSquare.posY), title: cacheSquare.title!, content: cacheSquare.content!, dopContent: cacheSquare.dopContent!, color: UIColor(hex: cacheSquare.color!)!, confirm: cacheSquare.confirm)
          
          self.uploadedSquares.append(square)
          self.loadSquaresToCollection(collectionVC)
        }
        DDLogInfo("* Upload squares list complete.")
      } catch {
        DDLogError("*Upload squares. Error result!")
      }
    }
    //upload headers
    self.objectContext?.performAndWait {
      let request: NSFetchRequest<HeadersEntity> = HeadersEntity.fetchRequest()
      let sort = NSSortDescriptor(key: "sort", ascending: true)
      request.predicate = pred
      request.sortDescriptors = [sort]
      do {
        guard let result = try self.objectContext?.fetch(request) else {
          DDLogError("* Upload headers. Error result!")
          return
        }
        var uploadedHeaders = [String]()
        for header in result {
          
          uploadedHeaders.append(header.name!)
        }
        self.loadHeadersToCollection(names: uploadedHeaders, collectionVC: collectionVC)
        DDLogInfo("* Upload headers list complete.")
      } catch {
        DDLogError("*Upload headers. Error result!")
      }
    }
    
  }
  
  func loadHeadersToCollection(names: [String], collectionVC: CollectionVC) {
    if names.count == collectionVC.headerNames.count {
      collectionVC.headerNames = names
      collectionVC.reloadCollection()
    }
    collectionVC.stopSpinner()
  }
  
  func loadSquaresToCollection(_ collectionVC: CollectionVC) {
    for sq:Square in uploadedSquares {
      collectionVC.squares[sq.posX][sq.posY] = sq
    }
    collectionVC.reloadCollection()
  }
}

