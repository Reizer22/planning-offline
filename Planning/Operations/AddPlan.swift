//
//  AddPlan.swift
//  Planning
//
//  Created by admin on 04.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class AddPlan: CacheOperation {
  
  func insert(_ plan: Plan) {
    let plansVC = controller as! PlansVC
    plansVC.startSpinner()
    insertToCoreData(plan)
    insertToArray(plan,controller: plansVC)
    plansVC.stopSpinner()
    plansVC.tableView.reloadData()
  }
  
  func insertToCoreData(_ plan: Plan) {
    
    self.objectContext?.performAndWait {
      guard let context = self.objectContext,
        let description = NSEntityDescription.entity(forEntityName: "PlanEntity", in: context)
        else {
          DDLogError("* InsertToCoreData. Error in description!")
          return
      }
      let planCache = PlanEntity(entity: description, insertInto: self.objectContext)
      planCache.uid = plan.uid
      planCache.name = plan.name
      planCache.iconName = plan.iconName
      planCache.startDate = plan.startDate as NSDate
      planCache.contentTitle = plan.contentTitle
      planCache.dopContentTitle = plan.dopContentTitle
      do {
        try self.objectContext?.save()
        DDLogInfo("* InsertToCoreData. Insert complete.")
      } catch {
        DDLogError("* Plan not save in core data!")
      }
    }
  }
  
  func insertToArray(_ plan: Plan, controller: PlansVC) {
    
    controller.plans.append(plan)
  }
}

