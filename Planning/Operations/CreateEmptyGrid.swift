//
//  CreateEmptyGrid.swift
//  Planning
//
//  Created by admin on 25.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class CreateEmptyGrid: NSObject {
  
  init(controller: CollectionVC) {
    var headerDate = controller.selectedPlan.startDate.addingTimeInterval(-60*60*24)
    for i in 0...30 {
      controller.squares.append([String]())
      controller.headerNames.append("-")
      for  k in 0...90 {
        controller.headerDates.append(headerDate)
        headerDate = headerDate.addingTimeInterval(60*60*24)
        controller.squares[i].append("")
        //set position collection view
        if headerDate.daysBetweenDate(toDate: Date()) == 0 {
          controller.currentDateRow = k
        }
      }
    }
  }
  
}

