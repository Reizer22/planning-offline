//
//  CreateCollectionCell.swift
//  Planning
//
//  Created by admin on 25.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class CreateCollectionCell: NSObject {
  
  let horizontalCellIdentifier = "HorizontalCellIdentifier"
  let verticalCellIdentifier = "VerticalCellIdentifier"
  var collectionVC:CollectionVC!
  
  init(collectionVC: CollectionVC) {
    self.collectionVC = collectionVC
    
    collectionVC.collectionView.register(UINib(nibName: "VerticalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: verticalCellIdentifier)
    collectionVC.collectionView.register(UINib(nibName: "HorizontalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: horizontalCellIdentifier)
  }
  
  func cellOn(collectionView: UICollectionView, withIndex indexPath:IndexPath) -> UICollectionViewCell {
    
    if indexPath.row != 0 && indexPath.section != 0 {
      return contentCell(indexPath: indexPath)
    }
    return headersCell(indexPath: indexPath)
  }
  
  func contentCell(indexPath: IndexPath) ->UICollectionViewCell {
    
    let horizontalCell : HorizontalCollectionViewCell = collectionVC.collectionView.dequeueReusableCell(withReuseIdentifier: horizontalCellIdentifier, for: indexPath) as! HorizontalCollectionViewCell
    
    horizontalCell.confirmImageView.image = nil
    
    if collectionVC.squares[indexPath.section][indexPath.row] is Square {
      let sq = collectionVC.squares[indexPath.section][indexPath.row] as! Square
      horizontalCell.horizontalLabel.text = sq.title
      horizontalCell.backgroundColor = sq.color
      
      if sq.confirm {
        horizontalCell.confirmImageView.image = UIImage(named: "Ok-100.png")
      }
    }
    else {
      horizontalCell.horizontalLabel.text = ""
      if collectionVC.isEditing {
        horizontalCell.backgroundColor = UIColor(white: 180/255.0, alpha: 1.0)
      }
      else {
        horizontalCell.backgroundColor = UIColor(white: 230/255.0, alpha: 1.0)
      }
    }
    
    return horizontalCell
  }
  
  func headersCell(indexPath: IndexPath) -> UICollectionViewCell {
    
    let verticalCell : VerticalCollectionViewCell = collectionVC.collectionView.dequeueReusableCell(withReuseIdentifier: verticalCellIdentifier, for: indexPath) as! VerticalCollectionViewCell
    
    if indexPath.section == 0 {
      
      let format = DateFormatter()
      format.dateFormat = "dd.MM"
      
      if indexPath.row == 0 {
        verticalCell.verticalLabel.text = "Object\\Date"
      }
      else {
        verticalCell.verticalLabel.text = format.string(from: collectionVC.headerDates[indexPath.row])
      }
      format.dateFormat = "EEE"
      let currentWeek = format.string(from: collectionVC.headerDates[indexPath.row])
      switch currentWeek {
      case "сб","вс","Sat","Sun":
        verticalCell.backgroundColor = #colorLiteral(red: 1, green: 0.3819925882, blue: 0.3135913318, alpha: 1)
      default:
        verticalCell.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
      }
      if indexPath.row != 0 {
        verticalCell.verticalLabel.text = verticalCell.verticalLabel.text!+", "+currentWeek
      }
      format.dateFormat = "dd.MM.yyyy"
      
      if format.string(from: Date()) == format.string(from: collectionVC.headerDates[indexPath.row]) {
        verticalCell.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
      }
      
      return verticalCell
      
    }
    else {
      if collectionVC.headerNames[indexPath.section] == "-" {
        verticalCell.verticalLabel.text = ""
      }
      else {
        verticalCell.verticalLabel.text = collectionVC.headerNames[indexPath.section]
      }
      if indexPath.section % 2 != 0 {
        verticalCell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
      } else {
        verticalCell.backgroundColor = UIColor.white
      }
      return verticalCell
    }
  }
}

