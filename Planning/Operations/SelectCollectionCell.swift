//
//  SelectCollectionCell.swift
//  Planning
//
//  Created by admin on 25.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import Popover

class SelectCollectionCell: NSObject {
  
  var collectionVC: CollectionVC!
  let popover = Popover()
  var selectedLineView: UIView?
  
  init(controller: CollectionVC) {
    collectionVC = controller
  }
  
  func longPressOnCell(_ indexPath: IndexPath) {
    print("longpress")
  }
  
  func highlightSelectedLineOn(_ indexPath: IndexPath) {
    //if selected view is created, remove from collectionView
    if let selected = selectedLineView {
      selected.removeFromSuperview()
    }
    
    if indexPath.section == 0 {
      print("* tap on header horizontal")
      selectedLineView = UIView(frame: CGRect(x: (collectionVC.collectionView.cellForItem(at: indexPath)?.frame.minX)!, y: -200, width: (collectionVC.collectionView.cellForItem(at: indexPath)?.frame.width)!, height: collectionVC.collectionView.contentSize.height+200))
    }
    else {
      print("* tap on header vertical")
      selectedLineView = UIView(frame: CGRect(x: -200, y: (collectionVC.collectionView.cellForItem(at: indexPath)?.frame.minY)!, width: collectionVC.collectionView.contentSize.width+200, height: (collectionVC.collectionView.cellForItem(at: indexPath)?.frame.height)!))
    }
    selectedLineView?.backgroundColor = UIColor.black
    selectedLineView?.alpha = 0.3
    
    collectionVC.collectionView.addSubview(selectedLineView!)
  }
  
  func tapOnCell(indexPath: IndexPath, popoverPoint: CGPoint) {
    
    if indexPath.section == 0 || (indexPath.row == 0 && collectionVC.isEditing == false) {
      highlightSelectedLineOn(indexPath)
    }
      // tap to header with editmode
    else if indexPath.row == 0 && collectionVC.isEditing {
      
      let alert = UIAlertController(title: "Rename cell", message: "Enter text", preferredStyle: .alert)
      alert.addTextField { (textField) in
        if self.collectionVC.headerNames[indexPath.section] != "-" {
          textField.text = self.collectionVC.headerNames[indexPath.section]
        }
      }
      
      alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { [weak alert] (_) in
        let textField = alert?.textFields![0]
        _ = RenameHeader(controller: self.collectionVC, newName: (textField?.text)!, row: indexPath.section)
      }))
      
      collectionVC.present(alert, animated: true, completion: nil)
      
    }
      // tap to content
    else {
      
      collectionVC.selectedIndexPath = indexPath
      
      if collectionVC.isEditing {
        
        collectionVC.popoverView?.isHidden = false
        collectionVC.popoverView?.backgroundColor = UIColor.clear
        //popover.popoverColor = UIColor.clear
        collectionVC.popoverView?.frame = CGRect(x: (collectionVC.popoverView?.frame.origin.x)!, y: (collectionVC.popoverView?.frame.origin.y)!, width: (collectionVC.popoverView?.frame.width)!, height: 220)
        
        if popoverPoint.y+220 >= collectionVC.collectionView.frame.height {
          popover.popoverType = .up
        }
        else {
          popover.popoverType = .down
        }
        
        popover.show(collectionVC.popoverView!, point: popoverPoint, inView: collectionVC.view)
      }
      else {
        if let indexPath = collectionVC.selectedIndexPath {
          if collectionVC.squares[indexPath.section][indexPath.row] is Square {
            let square = (collectionVC.squares[indexPath.section][indexPath.row] as! Square)
            
            collectionVC.previewContentLabel.text = collectionVC.selectedPlan.contentTitle
            collectionVC.previewDopContentLabel.text = collectionVC.selectedPlan.dopContentTitle
            collectionVC.previewContentText.text = square.content
            collectionVC.previewDopContentText.text = square.dopContent
            
            collectionVC.popoverPreView?.isHidden = false
            collectionVC.popoverPreView?.backgroundColor = UIColor.clear
            //popover.popoverColor = UIColor.clear
            collectionVC.popoverPreView?.frame = CGRect(x: (collectionVC.popoverPreView?.frame.origin.x)!, y: (collectionVC.popoverPreView?.frame.origin.y)!, width: (collectionVC.popoverPreView?.frame.width)!, height: 230)
            
            if popoverPoint.y+230 >= collectionVC.collectionView.frame.height {
              popover.popoverType = .up
            }
            else {
              popover.popoverType = .down
            }
            
            popover.show(collectionVC.popoverPreView!, point: popoverPoint, inView: collectionVC.view)
          }
        }
        
        // collectionVC.openSquare((Any).self)
      }
    }
  }
}

