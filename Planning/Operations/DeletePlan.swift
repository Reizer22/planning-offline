//
//  DeletePlan.swift
//  Planning
//
//  Created by admin on 04.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class DeletePlan: CacheOperation {
  
  func delete(atRow row:Int) {
    let plansVC = controller as! PlansVC
    self.deleteFromCoreData(idPlan: plansVC.plans[row].uid, plansVC: plansVC)
    self.deleteFromArray(row: row, plansVC: plansVC)
  }
  
  func deleteFromArray(row: Int, plansVC: PlansVC) {
    
    plansVC.plans.remove(at: row)
    
  }
  
  func deleteFromCoreData(idPlan: String?, plansVC: PlansVC) {
    if let id = idPlan {
      self.objectContext?.performAndWait {
        let pred = NSPredicate(format: "uid == %@", id.uppercased())
        let request: NSFetchRequest<PlanEntity> = PlanEntity.fetchRequest()
        request.predicate = pred
        do {
          if let planEntity = try self.objectContext?.fetch(request) {
            if planEntity.count > 0 {
              self.objectContext?.delete(planEntity[0])
              do {
                try self.objectContext?.save()
              } catch {
                
              }
              DDLogInfo("* Delete plan from CoreData complete.")
            }
            else {
              DDLogError("* DeleteFromCoreData. Plan is not found!")
            }
          }
        } catch {
          DDLogError("* DeleteFromCoreData. Error result")
        }
      }
    }
    else {
      plansVC.alert(withTitle: "Error", andText: "Id not found, delete failed! Reload app.")
    }
  }
}

