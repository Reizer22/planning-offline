//
//  GetPlans.swift
//  Planning
//
//  Created by admin on 05.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class GetPlans: CacheOperation {
  
  func uploadListFromCoreData() {
    let plansVC = controller as! PlansVC
    
    plansVC.startSpinner()
    
    self.objectContext?.performAndWait {
      let request: NSFetchRequest<PlanEntity> = PlanEntity.fetchRequest()
      do {
        guard let result = try self.objectContext?.fetch(request) else {
          DDLogError("* Upload plan list. Error result!")
          return
        }
        for cachePlan in result {
          
          let plan = Plan(uid: cachePlan.uid!, iconName: cachePlan.iconName!, name: cachePlan.name!, startDate: cachePlan.startDate! as Date, contentTitle: cachePlan.contentTitle, dopContentTitle: cachePlan.dopContentTitle)
          
          plansVC.plans.append(plan)
        }
        DDLogInfo("* Upload plan list complete.")
      } catch {
        DDLogError("*Upload plan. Error result!")
      }
      plansVC.stopSpinner()
      plansVC.tableView.reloadData()
    }
  }
}

