//
//  UpdateSquare.swift
//  Planning
//
//  Created by admin on 23.06.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class UpdateSquare: NSObject {
  
  init(square: Square, andReload collectVC:CollectionVC) {
    if square.title == "" {
      collectVC.squares[square.posX][square.posY] = (Any).self
    }
    else {
      collectVC.squares[square.posX][square.posY] = square
    }
    collectVC.reloadCollection()
  }
}

