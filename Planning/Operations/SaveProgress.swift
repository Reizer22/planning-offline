//
//  SaveProgress.swift
//  Planning
//
//  Created by admin on 06.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CocoaLumberjack
import CoreData

class SaveProgress: CacheOperation {
  
  var squares = [Square]()
  var planID: String!
  var headerNames = [String]()
  var editButton: UIBarButtonItem!
  
  var collectionVC: CollectionVC!
  
  func save(objects: [[Any]], plan: String, hNames: [String]) {
    
    let collectionVC = controller as! CollectionVC
    planID = plan
    headerNames = hNames
    editButton = collectionVC.editButton
    
    for obj:[Any] in objects {
      for element:Any in obj {
        if let el = element as? Square {
          squares.append(el)
        }
      }
    }
    self.uploadSquaresOnCoreData(collectionVC: collectionVC)
    self.uploadHeadersOnCoreData()
  }
  
  func uploadHeadersOnCoreData() {
    self.objectContext?.performAndWait {
      guard let context = self.objectContext,
        let description = NSEntityDescription.entity(forEntityName: "HeadersEntity", in: context)
        else {
          DDLogError("* uploadHeadersOnCoreData. Error in description!")
          return
      }
      let pred = NSPredicate(format: "idPlan == %@", self.planID.uppercased())
      let request: NSFetchRequest<HeadersEntity> = HeadersEntity.fetchRequest()
      request.predicate = pred
      do {
        if let headersEntity = try self.objectContext?.fetch(request) {
          for header in headersEntity {
            self.objectContext?.delete(header)
          }
        }
      } catch {}
      var sort = 0
      for name in self.headerNames {
        let cacheHeaders = HeadersEntity(entity: description, insertInto: self.objectContext)
        cacheHeaders.idPlan = self.planID
        cacheHeaders.name = name
        cacheHeaders.sort = Int32(sort)
        sort += 1
      }
      do {
        try self.objectContext?.save()
        DDLogInfo("* uploadHeadersOnCoreData. Upload complete.")
        
      } catch {
        DDLogError("* Headers not save in core data!")
      }
    }
  }
  
  func uploadSquaresOnCoreData(collectionVC: CollectionVC) {
    collectionVC.displayNavBarActivity()
    
    self.objectContext?.performAndWait {
      guard let context = self.objectContext,
        let description = NSEntityDescription.entity(forEntityName: "SquaresEntity", in: context)
        else {
          DDLogError("* uploadSquaresOnCoreData. Error in description!")
          return
      }
      let pred = NSPredicate(format: "idPlan == %@", self.planID.uppercased())
      let request: NSFetchRequest<SquaresEntity> = SquaresEntity.fetchRequest()
      request.predicate = pred
      do {
        if let squaresEntity = try self.objectContext?.fetch(request) {
          for square in squaresEntity {
            self.objectContext?.delete(square)
          }
        }
      } catch {}
      
      for square in self.squares {
        let cacheSquare = SquaresEntity(entity: description, insertInto: self.objectContext)
        cacheSquare.idPlan = self.planID
        cacheSquare.posX = Int16(square.posX)
        cacheSquare.posY = Int16(square.posY)
        cacheSquare.title = square.title
        cacheSquare.content = square.content
        cacheSquare.dopContent = square.dopContent
        cacheSquare.color = square.color.toHexString
        cacheSquare.confirm = square.confirm
      }
      do {
        try self.objectContext?.save()
        collectionVC.navigationItem.rightBarButtonItem = self.editButton
        DDLogInfo("* uploadSquaresOnCoreData. Upload complete.")
        
      } catch {
        DDLogError("* Squares not save in core data!")
      }
    }
  }
  
  func error(_ error: String!, code: Int32, severity: Int32) {
    print("error: \(error)")
  }
}

