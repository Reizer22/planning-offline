//
//  ZoomSquare.swift
//  Planning
//
//  Created by admin on 10.08.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class ZoomSquare: CacheOperation {
  
  var isUpdated = false
  
  func getSize() {
    self.objectContext?.performAndWait {
      let request: NSFetchRequest<ZoomEntity> = ZoomEntity.fetchRequest()
      do {
        guard let result = try self.objectContext?.fetch(request) else {
          DDLogError("* getSize error result")
          return
        }
        if result.count != 1 {
          DDLogError("* getSize error! result.count = \(result.count)")
        }
        else {
          (self.controller as! CollectionVC).setSquare(size: result[0].size)
          DDLogInfo("* load size complete")
        }
      } catch {
        DDLogError("* get size error result")
      }
    }
  }
  
  func setSize(_ size: Double) {
    self.objectContext?.performAndWait {
      let request: NSFetchRequest<ZoomEntity> = ZoomEntity.fetchRequest()
      do {
        if let zoomEntity = try self.objectContext?.fetch(request) {
          if zoomEntity.count > 0 {
            zoomEntity[0].size = size
            do {
              try self.objectContext?.save()
              self.isUpdated = true
            } catch {
              
            }
            DDLogInfo("* SaveZoomIntoCoreData")
            return
          }
          else {
            DDLogError("* Size is not found!")
          }
        }
      } catch {
        DDLogError("* set size error result")
        return
      }
    }
    if isUpdated == false {
      guard let context = self.objectContext,
        let description = NSEntityDescription.entity(forEntityName: "ZoomEntity", in: context)
        else {
          DDLogError("* Error in description!")
          return
      }
      let zoomEntity = ZoomEntity(entity: description, insertInto: self.objectContext)
      zoomEntity.size = size
      do {
        try self.objectContext?.save()
        DDLogInfo("* Save in cache complete.")
      } catch {
        DDLogError("* Not save in core data!")
      }
    }
  }
}

