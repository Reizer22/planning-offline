//
//  PopoverOperations.swift
//  Planning
//
//  Created by admin on 02.08.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class PopoverOperations: NSObject {
  
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  var collectionVC: CollectionVC!
  
  init(controller: CollectionVC) {
    collectionVC = controller
  }
  
  func confirm(indexPath:IndexPath, editMode:Bool, idPlan: String) {
    
    guard var square = collectionVC.squares[indexPath.section][indexPath.row] as? Square
      else {
        collectionVC.selectCell?.popover.dismiss()
        collectionVC.alert(withTitle: "Error", andText: "Confirm is failed!")
        return
    }
    collectionVC.selectCell?.popover.dismiss()
    
    if square.confirm {
      square.setConfirm(false)
    }
    else {
      square.setConfirm(true)
    }
    if !editMode {
      confirmOnDatabase(square.confirm, posX: square.posX, posY: square.posY, idPlan: idPlan)
    }
    collectionVC.squares[indexPath.section][indexPath.row] = square
    collectionVC.collectionView.reloadData()
  }
  
  func confirmOnDatabase(_ confirm: Bool, posX:Int, posY:Int, idPlan: String) {
    //confirm
  }
  
}

