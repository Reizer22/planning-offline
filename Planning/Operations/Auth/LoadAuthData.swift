//
//  LoadAuthData.swift
//  Planning
//
//  Created by admin on 13.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class LoadAuthData: CacheOperation {
  
  func main() {
    
    self.objectContext?.performAndWait {
      let request: NSFetchRequest<AuthEntity> = AuthEntity.fetchRequest()
      do {
        guard let result = try self.objectContext?.fetch(request) else {
          DDLogError("* error result")
          return
        }
        if result.count != 1 {
          DDLogError("* error! result.count = \(result.count)")
        }
        else {
          // (self.controller as! AuthVC).autoEnterWith(Login: result[0].login!, andPassword: result[0].password!)
          DDLogInfo("* loadCacheComplete")
        }
      } catch {
        DDLogError("* error result")
      }
    }
  }
}

