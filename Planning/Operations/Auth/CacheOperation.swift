//
//  CacheOperation.swift
//  Planning
//
//  Created by admin on 13.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class CacheOperation: NSObject {
  
  var coordinator: NSPersistentStoreCoordinator?
  var objectContext: NSManagedObjectContext?
  var controller: UIViewController!
  
  
  init(controller: UIViewController) {
    self.controller = controller
    guard let url = Bundle.main.url(forResource: "Model", withExtension: "momd") else {
      DDLogError("* URL not found!")
      return
    }
    guard let model = NSManagedObjectModel(contentsOf: url) else {
      DDLogError("* model is nil!")
      return
    }
    coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
    
    objectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    objectContext?.persistentStoreCoordinator = coordinator
    
    do {
      guard let pCoordinator = coordinator,
        let docurl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        else {
          return
      }
      let storeurl = docurl.appendingPathComponent("model.sqlite")
      _ = try pCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeurl, options: [NSMigratePersistentStoresAutomaticallyOption: NSNumber(value: true as Bool), NSInferMappingModelAutomaticallyOption: NSNumber(value: true as Bool)])
    } catch {
      DDLogError("* error request")
    }
  }
  
}

