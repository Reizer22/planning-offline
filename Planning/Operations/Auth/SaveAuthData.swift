//
//  SaveAuthData.swift
//  Planning
//
//  Created by admin on 13.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class SaveAuthData: CacheOperation {
  
  var isUpdated = false
  
  func saveOrUpdate(withLogin login: String, andPassword pass: String) {
    
    self.objectContext?.performAndWait {
      let pred = NSPredicate(format: "id == %d",1)
      let request: NSFetchRequest<AuthEntity> = AuthEntity.fetchRequest()
      request.predicate = pred
      do {
        if let authEntity = try self.objectContext?.fetch(request) {
          if authEntity.count > 0 {
            authEntity[0].setValuesForKeys(["login": login,"password": pass])
            do {
              try self.objectContext?.save()
              self.isUpdated = true
            } catch {
              
            }
            DDLogInfo("* UpdateInCacheComplete")
            return
          }
          else {
            DDLogError("* Is not found!")
          }
        }
      } catch {
        DDLogError("* error result")
        return
      }
    }
    if isUpdated == false {
      guard let context = self.objectContext,
        let description = NSEntityDescription.entity(forEntityName: "AuthEntity", in: context)
        else {
          DDLogError("* Error in description!")
          return
      }
      let authEntity = AuthEntity(entity: description, insertInto: self.objectContext)
      authEntity.id = 1
      authEntity.login = login
      authEntity.password = pass
      do {
        try self.objectContext?.save()
        DDLogInfo("* Save in cache complete.")
      } catch {
        DDLogError("* Not save in core data!")
      }
    }
  }
}

