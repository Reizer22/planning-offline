//
//  RenameHeader.swift
//  Planning
//
//  Created by admin on 07.07.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class RenameHeader: NSObject {
  
  init(controller: CollectionVC,newName: String, row: Int) {
    controller.headerNames[row] = newName
    controller.reloadCollection()
  }
}

